# serialMonitor

Simple application which allow to monitor serial port.

## Functions:

* Automatically detects active COM port
* Allow to log into a file (default folder for log files is: serialMonitorLogs)
* Autoscroll focus cursor on current received line

## Used libraries versions

* Python 3.7
* PyQt5 5.10.1
* pyserial 3.4

## Screenshots:

![Main window screenshoot](img/serialMonitor.png)
![Working app screenshoot](img/serialMonitor2.jpg)

